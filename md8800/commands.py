# MD8800 Display Driver: Control the VFD of the MD8800 PC using Python.
#     Copyright (C) 2014  Markus Koch <CClassicVideos@aol.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License version 3 as
#     published by the Free Software Foundation.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


# CONSTANTS (from lcdproc)
SYS_ESCAPE = 0x1B;
SYS_RESET = 0x1F;

TEXT_CLEAR = 0x50;
TEXT_LINEBOTH = 0x20;
TEXT_LINE1 = 0x21;
TEXT_LINE2 = 0x22;
TEXT_RESETCURSOR = 0x51; # Was 0x47 before, but 0x51 seems to be what we want...
TEXT_CR = 0x51; # Carriage return
TEXT_ON = 0x52; # Active line on
TEXT_OFF = 0x53; # Active line off (still writable, but hidden)

ICON = 0x30;
ICON_HDD = 0x0;
ICON_1394 = 0x1;
ICON_CD = 0x2;
ICON_USB = 0x3;
ICON_MOVIE = 0x4;
ICON_TV = 0x5;
ICON_MUSIC = 0x6;
ICON_PHOTO = 0x7;

ICON_RECORD = 0x8;

ICON_EMAIL = 0x9;
ICON_EMAILRED = 0xA;

ICON_VOLUME1 = 0xB;
ICON_VOLUME2 = 0xC;
ICON_VOLUME3 = 0xD;
ICON_VOLUME4 = 0xE;
ICON_VOLUME5 = 0xF;
ICON_VOLUME6 = 0x10;
ICON_VOLUME7 = 0x11;
ICON_VOLUMEMAX = 0x12;

ICON_SPEAKER = 0x13;
ICON_MUTE = 0x14;
ICON_WIFI1 = 0x15;
ICON_WIFI2 = 0x16;
ICON_WIFI3 = 0x17;

FRAME_HARDWARE = 0x18;
FRAME_MEDIA = 0x19;
FRAME_PLAYER = 0x1A;
FRAME_EMAIL = 0x1B;
FRAME_VOLUME = 0x1C;

BITMAP = 0x31;
BITMAP_HEIGHT = 7;
BITMAP_WIDTH = 9;

ANIMATION_CD = 0x32;
ANIMATION_RECORD = 0x33;
ANIMATION_WIFI = 0x34; # WIFI needs to be fully on, only 1 as speed can be used.

CLOCKSET = 0x00;
CLOCK_1224 = 0x01;
CLOCK_AMPM = 0x02;
CLOCK_STILL = 0x03;
CLOCK_ANIMATE = 0x04;
CLOCK_SHOW = 0x05;
