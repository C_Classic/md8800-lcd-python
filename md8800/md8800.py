# MD8800 Display Driver: Control the VFD of the MD8800 PC using Python.
#     Copyright (C) 2014  Markus Koch <CClassicVideos@aol.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License version 3 as
#     published by the Free Software Foundation.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


import serial;
import datetime;

from . import commands;

ser = "";

line = ["", ""];

image = [0, 0, 0, 0, 0, 0, 0, 0, 0]; # one number per column

default_brightness = 4;

# Command as integer, params as integer array
def sendCommand(command, params=[]):
    sendRaw(chr(commands.SYS_ESCAPE) + chr(command));
    for param in params:
       sendRaw(chr(param)); 

def sendRaw(data):
    ser.write(data.encode());

def init(port):
    global ser;
    ser = serial.Serial(port);
    ser.baudrate = 9600;
    sendCommand(commands.SYS_RESET);
    sendCommand(commands.TEXT_CLEAR);
    resetAllIcons();
    resetImage();

# Text interface
# Write directly to line[0,1] and call renderText();
# lines: 0, 1, 2 (=both)
# pad: left, right, center, none
# fixChars: BOOL
def renderText(lines = 2, pad = "left"):
    global line;
    if lines == 1 or lines == 0:
        if pad == "left":
            line[0] = '{: <16}'.format(line[0]); # Fix line width
        elif pad == "right":
            line[0] = '{: >16}'.format(line[0]); # Fix line width
        elif pad == "center":
            line[0] = '{: ^16}'.format(line[0]); # Fix line width
    if lines == 2 or lines == 1:
        if pad == "left":
            line[1] = '{: <16}'.format(line[1]); # Fix line width
        elif pad == "right":
            line[1] = '{: >16}'.format(line[1]); # Fix line width
        elif pad == "center":
            line[1] = '{: ^16}'.format(line[1]); # Fix line width
    render(lines);

def render(lines = 2):
    if lines == 2 or lines == 0:
        sendCommand(commands.TEXT_LINE1);
        sendCommand(commands.TEXT_RESETCURSOR);
        sendRaw(line[0]);
    if lines == 2 or lines == 1:
        sendCommand(commands.TEXT_LINE2);
        sendCommand(commands.TEXT_RESETCURSOR);
        sendRaw(line[1]);

def setTextMode(mode = "dual"):
    if (mode == 1):
        sendCommand(commands.TEXT_LINE1);
    elif (mode == 2):
        sendCommand(commands.TEXT_LINE2);
    else:
        sendCommand(commands.TEXT_LINEBOTH);

def writeText(text):
    sendRaw(text);

def clearText():
    sendCommand(commands.TEXT_CLEAR);


# CLOCK
# time: time as datetime variable
def setClock(time):
    time = datetime.datetime.now();
    sendCommand(commands.CLOCKSET, [int("0x" + str(time.minute),0), int("0x" + str(time.hour),0), int("0x" + str(time.day),0), int("0x" + str(time.month),0), int("0x" + str(time.year)[:2], 0), int("0x" + str(time.year)[2:], 0)]);
# style: "1224", "AMPM" or "last"
# animate: "on", "off" or "last"
def showClock(style = "last", animate = "last"):
    if style == "1224":
        sendCommand(commands.CLOCK_1224);
    elif style == "AMPM":
        sendCommand(commands.CLOCK_AMPM);
        
    if animate == "on":
        sendCommand(commands.CLOCK_ANIMATE);
    elif animate == "off":
        sendCommand(commands.CLOCK_STILL);
    
    sendCommand(commands.CLOCK_SHOW);
    
# ICONs
# brightness: 0-4 (off -> bright)
def setIcon(iconId, brightness = default_brightness):
    sendCommand(commands.ICON, [iconId, brightness]);
    
# speed: 0-4 (solid, fast -> slow)
def setAnimation(animationId, speed):
    sendCommand(animationId, [speed]);

# level: 0-9
def setVolume(level, brightness = default_brightness):
    if level > 8:
        level = 8;
    elif level < 0:
        level = 0;
    
    if level == 0:
        setIcon(commands.ICON_SPEAKER, 0);
        setIcon(commands.ICON_MUTE, brightness);
    else:
        setIcon(commands.ICON_MUTE, 0);
        setIcon(commands.ICON_SPEAKER, brightness);
    
    for i in range (0, level):
        setIcon(commands.ICON_VOLUME1 + i, brightness);
    for i in range (level, 8):
        setIcon(commands.ICON_VOLUME1 + i, 0);

def setWifi(level, brightness = default_brightness):
    if level > 3:
        level = 3;
    elif level < 0:
        level = 0;
    
    for i in range (0, level):
        setIcon(commands.ICON_WIFI1 + i, brightness);
    for i in range (level, 3):
        setIcon(commands.ICON_WIFI1 + i, 0);

def resetAllIcons():
    for iId in range (0x0, 0x1d):
        setIcon(iId, 0);


# EMAIL
# status 0 (off), 1 (white) or 2 (+red)
def setMail(status=1, brightness = default_brightness):
    mail=0;
    red=0;
    
    if status >= 1:
        mail = brightness;
    if status == 2:
        red = 1;
        
    setIcon(commands.ICON_EMAIL, mail);
    setIcon(commands.ICON_EMAILRED, red);


# BITMAP (Multimedia Matrix next to record)
def renderImage():
    global image;
    sendCommand(commands.BITMAP, image);

def setPixel(x,y,state=1):
    global image;
    if x > 8 or x < 0:
        print("[setPixel] Invalid X-coordinate.");
        return "ERR";
    if y > 6 or y < 0:
        print("[setPixel] Invalid Y-coordinate.");
        return "ERR";
    if state == 1:
        image[8-x] = image[8-x] | (1 << y);
    else:
        image[8-x] = image[8-x] & ~(1 << y);

def resetImage():
    for col in range(0,8):
        image[col] = 0;
    renderImage();
