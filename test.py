#!/usr/bin/env python

# MD8800 Display Driver: Control the VFD of the MD8800 PC using Python.
#     Copyright (C) 2014  Markus Koch <CClassicVideos@aol.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License version 3 as
#     published by the Free Software Foundation.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime;
import time;

from md8800 import md8800;
from md8800 import commands;


md8800.init("/dev/ttyUSB0");

md8800.setClock(datetime.datetime.now());

md8800.line[0] = "Hello";
md8800.line[1] = "world";

md8800.renderText(0, "center");
md8800.renderText(1, "center");

md8800.setIcon(md8800.commands.ICON_HDD);
md8800.setIcon(md8800.commands.ICON_MUSIC);

md8800.setIcon(md8800.commands.ICON_CD);
md8800.setIcon(md8800.commands.ICON_RECORD);
md8800.setIcon(md8800.commands.FRAME_MEDIA);
md8800.setIcon(md8800.commands.FRAME_HARDWARE);
md8800.setIcon(md8800.commands.FRAME_VOLUME);
md8800.setIcon(md8800.commands.FRAME_EMAIL);
md8800.setIcon(md8800.commands.FRAME_PLAYER);
md8800.setAnimation(md8800.commands.ANIMATION_CD, 1);
md8800.setAnimation(md8800.commands.ANIMATION_RECORD, 2);



for i in range (0,7):
    md8800.setPixel(i+1,i,1);
    md8800.setPixel(i+1, commands.BITMAP_HEIGHT - i-1,1);
md8800.renderImage();

for i in range (0, 4):
    md8800.setWifi(i);
    time.sleep(0.1);
    
md8800.setMail(1);

for i in range (0, 9):
    md8800.setVolume(i);
    time.sleep(0.1);

md8800.setMail(2);
    
time.sleep(2);    
md8800.showClock("1224", "off");

