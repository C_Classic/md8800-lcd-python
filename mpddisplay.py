#!/usr/bin/env python

# MD8800 Display Driver: Control the VFD of the MD8800 PC using Python.
#     Copyright (C) 2014  Markus Koch <CClassicVideos@aol.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License version 3 as
#     published by the Free Software Foundation.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime;
import time;
import json;
import threading;

from mpd import MPDClient

from md8800 import md8800;
from md8800 import commands;

# MEDIA STATUS
titleInfo = " "
titleInfo2 = "--:--";
titleInfo2old = "--:--OLD";

volume = 0;
volumeOld = 1;

# internal vars
currentIndex = 0;
updateFinished = 1;

playstat = "stop"; # stop, play, pause
playstatOld = "-";

OFF = 0;
ON = 4;


def stepTitleInfo():
	global currentIndex,titleInfo2, titleInfo2old, volume, volumeOld, playstat, playstatOld;
	threading.Timer(0.25, stepTitleInfo).start ();

	md8800.setTextMode(1);

	if (currentIndex >= len(titleInfo)):
		currentIndex = 0;

	md8800.writeText(titleInfo[currentIndex]);
	currentIndex = currentIndex + 1;

	if (titleInfo2 != titleInfo2old):
		titleInfo2old = titleInfo2;
		md8800.line[1] = titleInfo2;
		md8800.renderText(1, "right");

	if (volume != volumeOld):
		volumeOld = volume;
		md8800.setVolume(volume);

	if (playstat != playstatOld):
		playstatOld = playstat;
		if (playstat == "play"):
			md8800.setIcon(md8800.commands.ICON_CD);
			md8800.setAnimation(md8800.commands.ANIMATION_CD, 2);
		elif (playstat == "pause"):
			md8800.setIcon(md8800.commands.ICON_CD);
			md8800.setAnimation(md8800.commands.ANIMATION_CD, 0);
		else:
			md8800.setIcon(md8800.commands.ICON_CD, OFF);

def updateMPDstatus():
	global currentIndex, titleInfo, titleInfo2, client, updateFinished,volume, playstat;

	threading.Timer(1, updateMPDstatus).start ();

	if (updateFinished == 1):
		updateFinished = 0;

		title = client.currentsong();
		status = client.status();
		#print "STATUS: "
		#print status;

		newTI = "";

		value = title.get("artist", "");
		if (value != ""):
			newTI += value + " - ";
		else:
			value = title.get("albumartist", "");
			if (value != ""):
				newTI += value + " - ";

		newTI += title.get("title", "Unknown");

		value = title.get("album", "");
		if (value != ""):
			newTI += " (" + value + ")";

		newTI += " | ";

		#print "OLD: " + titleInfo;
		#print "NEW: " + newTI;

		if (newTI != titleInfo):
			titleInfo = newTI;
			currentIndex = 0;
			md8800.setTextMode(1);
			md8800.writeText('{: <16}'.format(""));

		# title progress
		tim = int(status.get("time", "0:0").split(":", 1)[0]);
		titleInfo2 = str(tim/60).zfill(2)  + ":" + str(tim%60).zfill(2) ;
		titleInfo2 = '{: >16}'.format(titleInfo2);
		temp = str(int(status.get("song","--"))+1) + "/" + status.get("playlistlength","--");
		titleInfo2 = temp + (titleInfo2[len(temp):16]);

		updateFinished = 1;

		# volume
		volume = ((int(status.get("volume", "0"))-20)/10);

		# status
		playstat = status.get("state");

	else:
		print("WARN: CPU too slow.")

def initDisplay():
	md8800.init("/dev/ttyAMA0");

	md8800.line[0] = "MPDClient 0.1b";
	md8800.line[1] = "Loading...";

	md8800.renderText(0, "center");
	md8800.renderText(1, "center");

	md8800.setClock(datetime.datetime.now());

	md8800.setIcon(md8800.commands.FRAME_VOLUME);
	md8800.setIcon(md8800.commands.FRAME_HARDWARE);
def initMPD():
	client.timeout = 10
	client.idletimeout = None
	client.connect("localhost", 6600)

def exitApp():
	global client;
	client.close()                     # send the close command
	client.disconnect()                # disconnect from the server


client = MPDClient();

initDisplay();
md8800.line[1] = "Connecting...";
md8800.renderText(2, "center");
initMPD();
md8800.line[1] = "";
md8800.renderText(2, "left");

#info = json.loads(client.currentsong());
#print info.get ("title", "error");




stepTitleInfo();
updateMPDstatus();

#print client.status();



#var = raw_input("Please enter something: ")
#exitApp();
#quit();



#md8800.setIcon(md8800.commands.ICON_HDD);

#md8800.setIcon(md8800.commands.FRAME_MEDIA);
#md8800.setIcon(md8800.commands.FRAME_HARDWARE);
#md8800.setIcon(md8800.commands.FRAME_VOLUME);
#md8800.setIcon(md8800.commands.FRAME_EMAIL);
#md8800.setIcon(md8800.commands.FRAME_PLAYER);
#md8800.setAnimation(md8800.commands.ANIMATION_CD, 1);
    
#time.sleep(2);    
#md8800.showClock("1224", "off");

# Now let's connect to the MPD server